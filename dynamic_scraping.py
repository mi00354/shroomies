#Imports Packages
from selenium import webdriver
from selenium.webdriver.common.by import By

# This is the path I use
# DRIVER_PATH = '.../Desktop/Scraping/chromedriver 2'
# Put the path for your ChromeDriver here
DRIVER_PATH = './chromedriver'
wd = webdriver.Chrome(executable_path=DRIVER_PATH)
wd.get('https://google.com')
wd.find_element(By.XPATH, '//*[@id="L2AGLb"]/div').click()

search_box = wd.find_element_by_css_selector('input.gLFyf')
search_box.send_keys('Dogs')

wd.quit()